# Build image
docker build . -f src/main/docker/Dockerfile.maven -t boosey/space-rest:latest

# Push image to Docker Hub
docker push  boosey/space-rest:latest

# Create new app - DC, Svc, ImageStream
oc new-app boosey/space-rest:latest
- DC: imagePullPolicy: Always
- Add in ImageStream: reference: true

# Route
oc expose svc/space-rest

# Update combined command
docker build . -f src/main/docker/Dockerfile.maven -t boosey/space-rest:latest \
  && docker push boosey/space-rest:latest \
  && oc rollout latest dc/space-rest