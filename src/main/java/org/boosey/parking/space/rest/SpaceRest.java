package org.boosey.parking.space.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import io.quarkus.grpc.runtime.annotations.GrpcService;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import org.boosey.parking.space.commands.MutinySpaceCommandsGrpc;
import org.boosey.parking.space.query.MutinySpaceQueryGrpc;
import org.boosey.parking.space.query.SpaceListReply;
import org.boosey.parking.space.query.SpaceListRequest;
import io.smallrye.mutiny.Uni;

@Path("/space")
public class SpaceRest {

    @Inject @GrpcService("space-query-service") MutinySpaceQueryGrpc.MutinySpaceQueryStub spaceQuery;
    @Inject @GrpcService("space-commands-service") MutinySpaceCommandsGrpc.MutinySpaceCommandsStub spaceCommands;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<String> listSpaces() {
        return spaceQuery.listSpaces(SpaceListRequest.newBuilder().build())
            .onItem()
            .apply(i -> printItem(i));
    }
    
    private String printItem(SpaceListReply slr) {
        try {
			return JsonFormat.printer().print(slr);
		} catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return "Error";
		}
    }    
    
}